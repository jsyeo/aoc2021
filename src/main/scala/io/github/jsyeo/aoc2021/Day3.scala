package io.github.jsyeo.aoc2021

import scala.io.Source

object Day3:
  def part1(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day3_input")
    part1Sol(Source.fromInputStream(input).getLines().toSeq)

  def findMax(s: String): Char =
    val counts = s.toCharArray.groupMapReduce(identity)(_ => 1)((x, y) => x + y)
    if (counts('0') < counts('1'))
      '1'
    else
      '0'

  def part1Sol(input: Seq[String], mask: Int = 0xfff): Unit =
    val bits = input
      .flatMap(s => s.toCharArray.zipWithIndex)
      .groupMapReduce(_._2)(_._1.toString)((x, y) => x + y)
    val gamma = Integer.parseInt(bits.view.mapValues(findMax).toSeq.sortBy(_._1).map(_._2).mkString, 2)
    val epsilon = ~gamma & mask
    println(gamma * epsilon)

  def part2(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day3_input")
    part2Sol(Source.fromInputStream(input).getLines().toSeq)

  def part2Sol(input: Seq[String]): Unit =
    def loop(input: Seq[String], prefer: Int, index: Int): String =
      if (input.size == 1)
        input(0)
      else
        val bits = input.groupMap(_.charAt(index))(identity)
        val char =
          if (bits('0').size < bits('1').size)
            (prefer + 48).toChar
          else if (bits('1').size < bits('0').size)
            ((prefer + 1) % 2 + 48).toChar
          else
            (prefer + 48).toChar
        loop(bits(char), prefer, index + 1)

    val o2 = Integer.parseInt(loop(input, 1, 0), 2)
    val co2 = Integer.parseInt(loop(input, 0, 0), 2)
    println(o2 * co2)

  def test(): Unit =
    val testStrings =
      """00100
        | 11110
        | 10110
        | 10111
        | 10101
        | 01111
        | 00111
        | 11100
        | 10000
        | 11001
        | 00010
        | 01010""".stripMargin('|').split("\n").map(_.trim)
    part1Sol(
    testStrings, 0x1f
    )
    part1()
    part2Sol(testStrings)

