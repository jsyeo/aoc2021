package io.github.jsyeo.aoc2021

import scala.io.Source


object Day2:
  case class Move(direction: String, distance: Int)

  def part1: Unit =
    case class Position(horizontal: Int, depth: Int)
    val input = this.getClass.getClassLoader.getResourceAsStream("day2_input")
    val finalPos = Source.fromInputStream(input)
      .getLines()
      .map(l => {
        val parts = l.split(" ")
        Move(parts(0), Integer.parseInt(parts(1)))
      })
      .foldLeft(Position(0, 0)) {
        case (pos, Move("forward", distance)) => pos.copy(horizontal = pos.horizontal + distance)
        case (pos, Move("up", distance)) => pos.copy(depth = pos.depth - distance)
        case (pos, Move("down", distance)) => pos.copy(depth = pos.depth + distance)
      }
    println(finalPos.depth * finalPos.horizontal)

  def part2: Unit =
    case class Position(horizontal: Int, depth: Int, aim: Int)
    val input = this.getClass.getClassLoader.getResourceAsStream("day2_input")
    val finalPos = Source.fromInputStream(input)
      .getLines()
      .map(l => {
        val parts = l.split(" ")
        Move(parts(0), Integer.parseInt(parts(1)))
      })
      .foldLeft(Position(0, 0, 0)) {
        case (pos, Move("forward", distance)) => pos.copy(horizontal = pos.horizontal + distance, depth = pos.depth + pos.aim * distance)
        case (pos, Move("up", distance)) => pos.copy(aim = pos.aim - distance)
        case (pos, Move("down", distance)) => pos.copy(aim = pos.aim + distance)
      }
    println(finalPos.depth * finalPos.horizontal)
