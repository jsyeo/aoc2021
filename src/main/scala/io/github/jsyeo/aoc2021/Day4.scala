package io.github.jsyeo.aoc2021

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

object Day4:
  def part1(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day4_input")
    val lines = Source.fromInputStream(input).getLines().toSeq
    part1Sol(lines)

  def part2(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day4_input")
    val lines = Source.fromInputStream(input).getLines().toSeq
    part2Sol(lines)

  def part1Sol(input: Seq[String]): Unit =
    case class Board(value: Array[Array[Int]]):
      def sumUnmarked(drawnNumbers: Set[Int]): Option[Int] = {
        val transposed = value.transpose
        val combinations = transposed ++ value
        val hasWon = combinations.exists(_.toSet.subsetOf(drawnNumbers))
        Option.when(hasWon)(value.flatten.toSet.diff(drawnNumbers).sum)
      }
    val bingoNums = input.head.split(",").map(Integer.parseInt).toSeq
    val pair = input.tail.foldLeft((List.empty[Board], ArrayBuffer.empty[Array[Int]])) {
      case ((boards, accumBoard), line) if line.isBlank =>
        if (accumBoard.nonEmpty)
          (Board(accumBoard.toArray) :: boards, ArrayBuffer.empty)
        else
          (boards, ArrayBuffer.empty)
      case ((boards, accumBoard), line) => (boards, accumBoard.append(line.split(" ").filter(!_.isBlank).map(Integer.parseInt)))
    }

    val boards = Board(pair._2.toArray) :: pair._1

    val (drawnNumbers, Some(winningBoard)) = bingoNums.foldLeft((ArrayBuffer.empty[Int], Option.empty[Board])) {
      case ((drawnNumbers, Some(sum)), _) => (drawnNumbers, Some(sum))
      case ((drawnNumbers, None), num) =>
        val numbers = drawnNumbers.append(num)
        if (numbers.size > 5)
          (numbers, boards.find(_.sumUnmarked(numbers.toSet).nonEmpty))
        else
          (numbers, None)
    }
    val last = drawnNumbers.last
    val value = winningBoard.sumUnmarked(drawnNumbers.toSet).get
    winningBoard.value.foreach(nums => println(nums.toList))
    println(last * value)

  def part2Sol(input: Seq[String]): Unit =
    case class Board(value: Array[Array[Int]]):
      def score(drawnNumbers: Set[Int], currentNumber: Int): Option[Int] =
        val transposed = value.transpose
        val combinations = transposed ++ value
        val allDrawn = Set(currentNumber).union(drawnNumbers)
        val hasWon = combinations.exists(_.toSet.subsetOf(allDrawn))
        Option.when(hasWon)(value.flatten.toSet.diff(allDrawn).sum * currentNumber)
    val bingoNums = input.head.split(",").map(Integer.parseInt).toSeq
    val pair = input.tail.foldLeft((List.empty[Board], ArrayBuffer.empty[Array[Int]])) {
      case ((boards, accumBoard), line) if line.isBlank =>
        if (accumBoard.nonEmpty)
          (Board(accumBoard.toArray) :: boards, ArrayBuffer.empty)
        else
          (boards, ArrayBuffer.empty)
      case ((boards, accumBoard), line) => (boards, accumBoard.append(line.split(" ").filter(!_.isBlank).map(Integer.parseInt)))
    }

    val boards = Board(pair._2.toArray) :: pair._1

    val (_, _, scores) = bingoNums.foldLeft((ArrayBuffer.empty[Int], Set.empty[Board], ArrayBuffer.empty[Int])) {
      case ((drawnNumbers, alreadyWon, scores), num) =>
        val numbers = drawnNumbers.append(num)
        if (numbers.size > 5) {
          val won = boards.toSet.diff(alreadyWon).filter(_.score(drawnNumbers.toSet, num).nonEmpty)
          (numbers, won.union(alreadyWon), scores ++ won.flatMap(_.score(drawnNumbers.toSet, num)).toArray)
        }
        else
          (numbers, alreadyWon, scores)
    }
    val value = scores.last
    println(value)

  def test(): Unit =
    val exampleInput =
      """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1
        |
        |22 13 17 11  0
        | 8  2 23  4 24
        |21  9 14 16  7
        | 6 10  3 18  5
        | 1 12 20 15 19
        |
        | 3 15  0  2 22
        | 9 18 13 17  5
        |19  8  7 25 23
        |20 11 10 24  4
        |14 21 16 12  6
        |
        |14 21 17 24  4
        |10 16 15  9 19
        |18  8 23 26 20
        |22 11 13  6  5
        | 2  0 12  3  7
        |""".stripMargin.split("\n").map(_.trim)
    Day4.part1Sol(exampleInput)
    Day4.part2Sol(exampleInput)
    Day4.part2()
