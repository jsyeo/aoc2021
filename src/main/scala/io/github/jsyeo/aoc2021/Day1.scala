package io.github.jsyeo.aoc2021

import scala.io.Source

object Day1:
  def countIncreased(list: Iterator[Int]): Int =
    list.foldLeft((Option.empty[Int], 0)) {
      case ((None, count), elem) => (Some(elem), count)
      case ((Some(prev), count), cur) if prev < cur => (Some(cur), count + 1)
      case ((Some(prev), count), cur) => (Some(cur), count)
    }._2

  def part1: Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day1_input")
    val timesIncreased = countIncreased(Source.fromInputStream(input).getLines().map(Integer.parseInt(_)))
    println(timesIncreased)

  def part1OneLiner: Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day1_input")
    val ints = Source.fromInputStream(input).getLines().map(Integer.parseInt(_)).toSeq
    println(ints.zip(ints drop 1).count(_ < _))

  def part2: Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day1_input")
    val ints = Source.fromInputStream(input).getLines().map(Integer.parseInt(_))
    println(countIncreased(ints.sliding(3).map(_.sum)))

  def part2OneLiner: Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day1_input")
    val ints = Source.fromInputStream(input).getLines().map(Integer.parseInt(_)).toSeq
    println(ints.zip(ints.drop(3)).count(_ < _))
