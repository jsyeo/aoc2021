package io.github.jsyeo.aoc2021

import scala.io.Source
import scala.jdk.CollectionConverters.*

object Day5:
  case class Position(x: Int, y: Int)
  case class Line(start: Position, end: Position)

  def test(): Unit =
    val exampleInput =
      """0,9 -> 5,9
        |8,0 -> 0,8
        |9,4 -> 3,4
        |2,2 -> 2,1
        |7,0 -> 7,4
        |6,4 -> 2,0
        |0,9 -> 2,9
        |3,4 -> 1,4
        |0,0 -> 8,8
        |5,5 -> 8,2""".stripMargin.lines()
    part2Sol(exampleInput.toList.asScala.toSeq)
    //    part1()
    part2()

  def genPositions(line: Line): List[Position] =
    if (line.start.x == line.end.x || line.start.y == line.end.y)
      (for {
        x <- if (line.start.x < line.end.x) line.start.x to line.end.x else line.end.x to line.start.x
        y <- if (line.start.y < line.end.y) line.start.y to line.end.y else line.end.y to line.start.y
      } yield Position(x, y)).toList
    else List.empty

  def part1(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day5_input")
    val lines = Source.fromInputStream(input).getLines().toSeq
    part1Sol(lines)

  def part2(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day5_input")
    val lines = Source.fromInputStream(input).getLines().toSeq
    part2Sol(lines)

  def part1Sol(input: Seq[String]): Unit =
    def parsePosition(s: String): Position =
      val parts = s.split(",").map(_.trim).map(Integer.parseInt)
      Position(parts(0), parts(1))

    val lines = input.map(line => {
      val positions = line.split("->").map(_.trim).map(parsePosition)
      Line(start = positions(0), end = positions(1))
    })

    val positions = lines.flatMap(genPositions)
    val counts = positions.groupMapReduce(identity)(_ => 1)(_ + _)
    println(counts.filter(_._2 > 1).size)


  def part2Sol(input: Seq[String]): Unit =
    def parsePosition(s: String): Position =
      val parts = s.split(",").map(_.trim).map(Integer.parseInt)
      Position(parts(0), parts(1))

    val lines = input.map(line => {
      val positions = line.split("->").map(_.trim).map(parsePosition)
      Line(start = positions(0), end = positions(1))
    })

    val positions = lines.flatMap(line => {
      if (Math.abs(line.start.x - line.end.x) == Math.abs(line.start.y - line.end.y))
        val xs = if (line.start.x < line.end.x) line.start.x to line.end.x else (line.end.x to line.start.x).toList.reverse
        val ys = if (line.start.y < line.end.y) line.start.y to line.end.y else (line.end.y to line.start.y).toList.reverse
        xs.zip(ys).map(Position(_, _))
      else genPositions(line)
    })
    val counts = positions.groupMapReduce(identity)(_ => 1)(_ + _)
    println(counts.filter(_._2 > 1).size)

