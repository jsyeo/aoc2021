package io.github.jsyeo.aoc2021

import scala.io.Source
import scala.collection.mutable.HashMap

object Main:
  def main(args: Array[String]): Unit =
//    Day6.part1Sol("3,4,3,1,2")
//    Day6.part1Sol("3")
//    Day6.part1()
//    Day6.part2Sol("3,4,3,1,2")
    Day6.part2()

object Day6:
  def part1(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day6_input")
    val lines = Source.fromInputStream(input).getLines().toSeq(0)
    part1Sol(lines)

  def part1Sol(input: String): Unit =
    val initial = input.split(",").filterNot(_.isBlank).map(Integer.parseInt).toSeq
    def next(state: Seq[Int]): Seq[Int] =
      state.foldLeft(List.empty[Int]) {
        case (accum, 0) => 6 :: 8 :: accum
        case (accum, fish) => (fish - 1) :: accum
      }.reverse

    val stream = LazyList.iterate(initial)(next)
    println(stream.take(18).map(_.head).count(_ == 0))

  def part2(): Unit =
    val input = this.getClass.getClassLoader.getResourceAsStream("day6_input")
    val lines = Source.fromInputStream(input).getLines().toSeq(0)
    part2Sol(lines)

  def part2Sol(input: String): Unit =
    def nextState(s: Int): Int = s match {
      case 0 => 6
      case s => s - 1
    }

    val initial = input.split(",").filterNot(_.isBlank).map(Integer.parseInt).toSeq
    def next(state: HashMap[Int, BigInt]): HashMap[Int, BigInt] =
      state.foldLeft(HashMap.empty[Int, BigInt]) {
        case (accum, (num, count)) if num == 0 =>
          val n = nextState(num)
          accum += (8 -> count) += (n -> (count + accum.getOrElse(n, 0)))
        case (accum, (num, count)) =>
          val n = nextState(num)
          accum += (n -> (count + accum.getOrElse(n, 0)))
      }

    val start = initial.foldLeft(HashMap.empty[Int, BigInt]) {
      case (acc, i) => acc += i -> (BigInt(1) + acc.getOrElse(i, 0))
    }
    val stream = LazyList.iterate(start)(next)
    stream.take(19).zipWithIndex.foreach(d => println(s"day ${d._2}: ${d._1}"))
    println(stream(256).values.sum)
